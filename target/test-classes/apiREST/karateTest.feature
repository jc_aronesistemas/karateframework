Feature:  Plan de pruebas

  @casoGet
  Scenario Outline: Caso de prueba<nombreCaso>

    Given url 'https://reqres.in/api/users/<ID>'
    When header 'Content-Type' = 'Aplication/json'
    * configure connectTimeout = 120000
    * configure readTimeout = 120000
    When method GET
    Then status <estadoHTTP>
    Examples:
      | nombreCaso           | ID |  | estadoHTTP |
      | caso id 1            | 1  |  | 200        |
      | caso id 2            | 2  |  | 200        |
      | caso id 3            | 3  |  | 200        |
      | caso id id not found | 50 |  | 404        |

  @casoPost
  Scenario Outline: caso de prueba json<nombreCaso2>
    Given url 'https://reqres.in/api/users'
    When request {"<name>": <Vname>,"<job>": <Vjob>}
    And method POST
    Then status 201
    Examples:
      | nombreCaso2    | name | Vname    | job | Vjob  |
      | name vacio     | name |          | job | "QA"  |
      | name nulo      | name | null     | job | "QA"  |
      | name 3 letras  | name | "sol"    | job | "QA"  |
      | sin campo name |      |          | job | "QA"  |
      | job vacio      | name | "jeremy" | job | ""    |
      | job nulo       | name | "jeremy" | job | null  |
      | job 3 letras   | name | "jeremy" | job | "QAs" |
      | sin campo job  | name | "jeremy" |     | ""    |


  Scenario: caso de pruebas tipo Post numero 2 cuando hay cantidad de datos
    Given url 'https://reqres.in/api/users'
    When request
    """
    {
    "name": "jeremy",
    "job": "QA"
}
    """
    And method POST
    Then status 201

  Scenario: Caso de prueba tipo DELETE
    Given url 'https://reqres.in/api/users/3'
    When method DELETE
    Then status 204
